//
//  FPAppData.swift
//  print
//
//  Created by Prakit Kongkaew on 3/18/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics
enum MJTextStyle: Int {
    case head0
    case head1
    case head2
    case head3
    case head4
    case head5
    case head6
    case head7
    
    case med1
    case med2
    case med3
    case med4
    
    case tiny1
    case tiny2
    case tiny3
    
    case thai1
    case thai2
    
    func font(regular: Bool = false) -> UIFont {
        switch self {
        case .head0: return UIFont.customFont(36, weight: DGTFontWeightBold)
        case .head1: return UIFont.customFont(30, weight: DGTFontWeightBold)
        case .head2: return UIFont.customFont(regular ? 36 : 24, weight: DGTFontWeightBold)
        case .head3: return UIFont.customFont(regular ? 36 : 24, weight: DGTFontWeightRegular)
        case .head4: return UIFont.customFont(22, weight: DGTFontWeightRegular)
        case .head5: return UIFont.customFont(regular ? 36 : 20, weight: DGTFontWeightBold)
        case .head6: return UIFont.customFont(regular ? 36 : 20, weight: DGTFontWeightMedium)
        case .head7: return UIFont.customFont(regular ? 36 : 20, weight: DGTFontWeightRegular)
            
        case .med1: return UIFont.customFont(regular ? 24 : 18, weight: DGTFontWeightBold)
        case .med2: return UIFont.customFont(regular ? 24 : 18, weight: DGTFontWeightRegular)
        case .med3: return UIFont.customFont(16, weight: DGTFontWeightRegular)
        case .med4: return UIFont.customFont(16, weight: DGTFontWeightBold)
            
        case .tiny1: return UIFont.customFont(regular ? 20 : 14, weight: DGTFontWeightRegular)
        case .tiny2: return UIFont.customFont(regular ? 18 : 12, weight: DGTFontWeightRegular)
        case .tiny3: return UIFont.customFont(regular ? 18 : 12, weight: DGTFontWeightBold)
            
        case .thai1: return UIFont(name: "Thonburi", size: 12)!
        case .thai2: return UIFont(name: "Thonburi", size: regular ? 18 : 14)!
        }
    }
}
enum FPside: Int {
    case left = 0
    case right
    
    func color () -> UIColor{
        switch self {
        case .left:
            return #colorLiteral(red: 0.2235294118, green: 0.6941176471, blue: 0.7215686275, alpha: 1)
        case .right:
            return #colorLiteral(red: 0.8549019608, green: 0.6078431373, blue: 0.4078431373, alpha: 1)
        }
    }
    func colorGradients () -> [UIColor]{
        switch self {
        case .left:
            return [#colorLiteral(red: 0.2235294118, green: 0.6941176471, blue: 0.7215686275, alpha: 1),#colorLiteral(red: 0.4039215686, green: 0.8549019608, blue: 0.8823529412, alpha: 1)]
        case .right:
            return [#colorLiteral(red: 0.8549019608, green: 0.6078431373, blue: 0.4078431373, alpha: 1),#colorLiteral(red: 0.9411764706, green: 0.7490196078, blue: 0.5921568627, alpha: 1)]
        }
    }
}
struct MJColorStyle {
    
    static let white = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let gold = #colorLiteral(red: 0.9411764706, green: 0.8588235294, blue: 0.7137254902, alpha: 1)
    static let lightbrown = #colorLiteral(red: 0.5411764706, green: 0.462745098, blue: 0.3176470588, alpha: 1)
    static let gray = #colorLiteral(red: 0.4039215686, green: 0.4039215686, blue: 0.4039215686, alpha: 1)
    static let brown = #colorLiteral(red: 0.2235294118, green: 0.1568627451, blue: 0.1058823529, alpha: 1)
    static let red = #colorLiteral(red: 0.662745098, green: 0, blue: 0, alpha: 1)
    
    static let goldGradients: [UIColor] = [#colorLiteral(red: 0.8196078431, green: 0.7333333333, blue: 0.5254901961, alpha: 1), #colorLiteral(red: 0.4549019608, green: 0.3921568627, blue: 0.2509803922, alpha: 1)]
    static let blackGradients: [UIColor] = [#colorLiteral(red: 0.2666666667, green: 0.2666666667, blue: 0.2666666667, alpha: 1), #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1)]
    
    static let linegray = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
    static let linebrown = #colorLiteral(red: 0.4549019608, green: 0.3921568627, blue: 0.2509803922, alpha: 1)
    static let linegold = #colorLiteral(red: 0.9411764706, green: 0.8588235294, blue: 0.7137254902, alpha: 1)
    static let linedarkgray = #colorLiteral(red: 0.07450980392, green: 0.07450980392, blue: 0.07450980392, alpha: 1)
    
    static let bgblack = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let bggray = #colorLiteral(red: 0.1215686275, green: 0.1215686275, blue: 0.1215686275, alpha: 1)
    static let bglightgray = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
}

class MJAppStyle {
    
    class var cornerRedius: DGTCornerInset {
        return DGTCornerInset(radius: 1)
    }
    
    class var availableSeatColor: [UIColor] {
        return [#colorLiteral(red: 0.7568627451, green: 0.1529411765, blue: 0.1764705882, alpha: 1),#colorLiteral(red: 0.5764705882, green: 0.1529411765, blue: 0.5607843137, alpha: 1),#colorLiteral(red: 0.9450980392, green: 0.3529411765, blue: 0.1411764706, alpha: 1),#colorLiteral(red: 0.9843137255, green: 0.6901960784, blue: 0.231372549, alpha: 1),#colorLiteral(red: 0.1607843137, green: 0.6705882353, blue: 0.8862745098, alpha: 1),#colorLiteral(red: 0.2235294118, green: 0.7098039216, blue: 0.2901960784, alpha: 1),#colorLiteral(red: 0.9294117647, green: 0.1176470588, blue: 0.4745098039, alpha: 1),#colorLiteral(red: 0, green: 0.4431372549, blue: 0.462745098, alpha: 1),#colorLiteral(red: 0.4588235294, green: 0.2980392157, blue: 0.1411764706, alpha: 1)]
    }
    
    class func lineGradient(for width: CGFloat, color: UIColor) -> UIImage? {
        let size = CGSize(width: width, height: 1)
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let currentContext = UIGraphicsGetCurrentContext()
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let gradientOption = CGGradientDrawingOptions(rawValue: 0)
        
        let startComponent = color.components
        
        let componentCount: Int = 2
        let components: [CGFloat] = [
            startComponent.red, startComponent.green, startComponent.blue, startComponent.alpha,
            startComponent.red, startComponent.green, startComponent.blue, 0,
            ]
        
        let locations: [CGFloat] = [0, 1]
        let gradient = CGGradient(colorSpace: colorSpace, colorComponents: components, locations: locations, count: componentCount)
        let centerPoint = CGPoint(x: width/2, y: 0.5)
        
        currentContext?.drawRadialGradient(gradient!, startCenter: centerPoint, startRadius: 0, endCenter: centerPoint, endRadius: width/2, options: gradientOption)
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
}


