//
//  FPAppUtility.swift
//  print
//
//  Created by Prakit Kongkaew on 3/26/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics


struct AppStoryboardIdentifier {
    static let select = "ImportStoryboardIdentifier"
    static let menu = "MenuStoryboardIdentifier"
    static let edit = "EditImageIdentifier"
}
enum direction : Int {
    
    case N = 0
    case NE
    case E
    case SE
    case S
    case SW
    case W
    case NW
    case CC
    
}


class rectunglePoint {
    var size : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    var begenPoint : CGPoint = CGPoint(x: 0, y: 0)
    var direc : direction? = nil
    func setSize(size : CGRect)  {
        self.size = size
        print(size)
    }
    func setPoint(point : CGPoint)  {
        begenPoint = point
        if around(point1: point, point2: pointN) {
            direc = .N
        }else if around(point1: point, point2: pointNE){
            direc = .NE
        }
        else if around(point1: point, point2: pointE){
            direc = .E
        }
        else if around(point1: point, point2: pointSE){
            direc = .SE
        }
        else if around(point1: point, point2: pointS){
            direc = .S
        }
        else if around(point1: point, point2: pointSW){
            direc = .SW
        }
        else if around(point1: point, point2: pointW){
            direc = .W
        }
        else if around(point1: point, point2: pointNW){
            direc = .NW
        }else if around(point1: point, point2: pointCC){
            direc = .CC
        }else {
            direc = nil
        }
   
    }
    var pointN :CGPoint {
        get {
            return CGPoint(x: size.midX, y: size.minY)
        }
    }
    var pointNE :CGPoint {
        get {
            return CGPoint(x: size.maxX, y: size.minY)
        }
    }
    var pointE :CGPoint {
        get {
            return CGPoint(x: size.maxX, y: size.midY)
        }
    }
    var pointSE :CGPoint {
        get {
            return CGPoint(x: size.maxX, y: size.maxY)
        }
    }
    var pointS :CGPoint {
        get {
            return CGPoint(x: size.midX, y: size.maxY)
        }
    }
    var pointSW :CGPoint {
        get {
            return CGPoint(x: size.minX, y: size.maxY)
        }
    }
    var pointW :CGPoint {
        get {
            return CGPoint(x: size.minX, y: size.midY)
        }
    }
    var pointNW :CGPoint {
        get {
            return CGPoint(x: size.minX, y: size.minY)
        }
    }
    var pointCC : CGPoint {
        get {
            return CGPoint(x: size.midX, y: size.midY)
        }
    }
    func around(point1 : CGPoint , point2:CGPoint  ) -> Bool {
        let dis = distance(point1,point2)
        print(point1,point2,dis)
        return dis < 25
    }
    func distance(_ a: CGPoint, _ b: CGPoint) -> CGFloat {
        let xDist = a.x - b.x
        let yDist = a.y - b.y
        return CGFloat(sqrt((xDist * xDist) + (yDist * yDist)))
    }
    
}
