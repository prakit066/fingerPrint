//
//  FPCustomUI.swift
//  print
//
//  Created by Prakit Kongkaew on 3/20/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

import UIKit
import Foundation
import QuartzCore
import CoreGraphics

struct ScreenSize
{
    static let width = UIScreen.main.bounds.width
    static let height = UIScreen.main.bounds.height
    static let max_length = max(width, height)
    static let min_length = min(width, height)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  IS_IPHONE && ScreenSize.max_length < 568.0
    static let IS_IPHONE_5 = IS_IPHONE && ScreenSize.max_length == 568.0
    static let IS_IPHONE_6 = IS_IPHONE && ScreenSize.max_length == 667.0
    static let IS_IPHONE_6P = IS_IPHONE && ScreenSize.max_length == 736.0
    
    static let IS_IPHONE =  UIDevice.current.userInterfaceIdiom == .phone
    static let IS_IPAD =  UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPAD_PRO = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.height == 1366
}

public enum DGTTintImageDirection: Int {
    case horizontal
    case vertical
}

public struct DGTCornerInset {
    var topleft: CGFloat
    var topright: CGFloat
    var bottomleft: CGFloat
    var bottomright: CGFloat
}

extension DGTCornerInset : Equatable {
    init() {
        self.topleft = 0
        self.topright = 0
        self.bottomleft = 0
        self.bottomright = 0
    }
    
    var isRound: Bool {
        return DGTCornerInset(radius: topleft) == self
    }
    
    static var zero: DGTCornerInset {
        return DGTCornerInset(radius: 0)
    }
    
    var resizableSize: CGSize {
        let width = max(topleft+topright, bottomleft+bottomright)
        let height = max(topleft+bottomleft, topright+bottomright)
        return CGSize(width: width+1, height: height+1)
    }
    
    public init(radius: CGFloat) {
        self.init(topleft: radius, topright: radius, bottomleft: radius, bottomright: radius)
    }
}

extension DGTCornerInset : CustomStringConvertible {
    public var description: String {
        get {
            return "DGTCornerInset { topleft: \(topleft), topright: \(topright), bottomleft: \(bottomleft), bottomright: \(bottomright) }"
        }
    }
}

public func == (lhs: DGTCornerInset, rhs: DGTCornerInset) -> Bool {
    return (lhs.topleft == rhs.topleft &&
        lhs.topright == rhs.topright &&
        lhs.bottomleft == rhs.bottomleft &&
        lhs.bottomright == rhs.bottomright)
}

public func DGTCornerInsetMake(_ topleft: CGFloat, topright: CGFloat, bottomleft: CGFloat, bottomright: CGFloat) -> DGTCornerInset {
    return DGTCornerInset(topleft: topleft, topright: topright, bottomleft: bottomleft, bottomright: bottomright)
}

public func DGTCornerInsetResizableSize(_ cornerInset: DGTCornerInset) -> CGSize {
    let width: CGFloat = max(cornerInset.topleft+cornerInset.topright, cornerInset.bottomleft+cornerInset.bottomright)
    let height: CGFloat = max(cornerInset.topleft+cornerInset.bottomleft, cornerInset.topright+cornerInset.bottomright)
    return CGSize(width: width, height: height)
}

public func DGTCornerInsetIsRound(_ cornerInset: DGTCornerInset) -> Bool {
    return DGTCornerInset(radius: cornerInset.topleft) == cornerInset
}

public var DGTCornerInsetZero: DGTCornerInset {
    return DGTCornerInset(topleft: 0, topright: 0, bottomleft: 0, bottomright: 0)
}

extension UIViewController {
    
    ///
    /// Returns a Boolean value for make sure UIViewcontroller is already visible.
    /// @return YES if the receiver is already visible, NO otherwise.
    ///
    public func isVisible() -> Bool {
        
        if let controller = self.parent?.presentedViewController?.presentingViewController {
            return !controller.isEqual(self)
        }
        if let controller = self.parent?.presentingViewController?.presentedViewController {
            return !controller.isEqual(self)
        }
        return (self.isViewLoaded && self.view.window != nil)
    }
    
    public func isLandscape() -> Bool {
        return view.frame.size.width > view.frame.height
    }
}

extension UIColor {
    
    public convenience init(grayscalCode: Int, alpha: CGFloat = 1) {
        self.init(red: (CGFloat(grayscalCode) / 255), green: (CGFloat(grayscalCode) / 255), blue: (CGFloat(grayscalCode) / 255), alpha: alpha)
    }
    
    public convenience init(r: Int, g: Int, b: Int, alpha: CGFloat = 1) {
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: alpha)
    }
    
    public convenience init(hex: String) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        let length = cString.characters.count
        if length == 2 {
            let grayscale = CGFloat(rgbValue & 0xFF) / 255.0
            self.init(red: grayscale, green: grayscale, blue: grayscale, alpha: 1.0)
        } else if length != 6 {
            let component = UIColor.gray.components
            self.init(red: component.red, green: component.green, blue: component.blue, alpha: component.alpha)
        } else {
            self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                      green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                      blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                      alpha: 1.0)
        }
    }
}

private var cache: NSCache<AnyObject, AnyObject>?

private var kDGTImageName = "DGTImageName"
private let kDGTImageSize = "DGTImageSize"
private let kDGTImageColors = "DGTImageColors"
private let kDGTImageBorderColor = "DGTImageBorderColor"
private let kDGTImageBorderWidth = "DGTImageBorderWidth"
private let kDGTImageShawdowColor = "DGTImageShawdowColor"
private let kDGTImageTintColor = "DGTImageTintColor"
private let kDGTImageTintStyle = "DGTImageTintStyle"
private let kDGTImageCornerInset = "DGTImageCornerInset"

extension UIImage {
    
    fileprivate struct AssociatedKeys {
        static var cornerInset = "cornerInset"
    }
    
    fileprivate(set) var cornerInset: DGTCornerInset {
        get {
            guard let value = objc_getAssociatedObject(self, &AssociatedKeys.cornerInset) as? NSValue else {
                return DGTCornerInset.zero
            }
            return value.dgtCornerInsetValue()
        }
        
        set(value) {
            objc_setAssociatedObject(self,&AssociatedKeys.cornerInset, NSValue(cornerInset: value),objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    
    @objc(imageName:tintColor:)
    public convenience init?(named: String, tintColor: UIColor) {
        if let image = UIImage(named: named) {
            let tintImage = image.tinted(tintColor)
            self.init(cgImage: tintImage.cgImage!, scale: tintImage.scale, orientation: tintImage.imageOrientation)
        } else {
            return nil
        }
    }
    
    @objc(imageWithLayer:)
    public convenience init?(layer: CALayer?) {
        if let layer = layer {
            UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, 0.0)
            guard let context = UIGraphicsGetCurrentContext() else { return nil }
            layer.render(in: context)
            guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
            UIGraphicsEndImageContext()
            self.init(cgImage: image.cgImage!, scale: image.scale, orientation: .up)
        } else {
            return nil
        }
    }
    
    @objc(imageWithColor:size:borderColor:borderWidth:cornerRadius:)
    public convenience init?(color: UIColor, size: CGSize, borderColor: UIColor? = nil, borderWidth: CGFloat = 0, cornerRadius: CGFloat) {
        if size.equalTo(CGSize.zero) {
            return nil
        }
        self.init(color: color, size: size, borderColor: borderColor, borderWidth: borderWidth, cornerInset: DGTCornerInset(radius: cornerRadius))
    }
    
    //    @objc(imageWithColor:size:borderColor:borderWidth:cornerInset:)
    public convenience init?(color: UIColor, size: CGSize, borderColor: UIColor? = nil, borderWidth: CGFloat = 0, cornerInset: DGTCornerInset = DGTCornerInsetZero) {
        
        var dict = Dictionary <String, AnyObject> ()
        dict[kDGTImageColors] = color
        dict[kDGTImageSize] = NSValue(cgSize: size)
        dict[kDGTImageBorderColor] = borderColor
        dict[kDGTImageBorderWidth] = NSValue(nonretainedObject: borderWidth)
        dict[kDGTImageCornerInset] = NSValue(cornerInset: cornerInset)
        
        let scale = UIScreen.main.scale
        
        if cornerInset == DGTCornerInsetZero {
            
            if borderColor == nil && borderWidth == 0 {
                var rect = CGRect()
                rect.size = size
                
                if size.equalTo(CGSize.zero) {
                    return nil
                }
                UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
                guard let context = UIGraphicsGetCurrentContext() else { return nil }
                color.setFill()
                context.fill(rect)
                
                guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
                UIGraphicsEndImageContext()
                
                self.init(cgImage: image.cgImage!, scale: scale, orientation: .up)
                return
            }
            
            var rect: CGRect = CGRect()
            rect.size = size;
            
            if size.equalTo(CGSize.zero) {
                self.init()
                return nil
            }
            
            UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
            guard let context = UIGraphicsGetCurrentContext() else { return nil }
            if let borderColor = borderColor {
                borderColor.setStroke()
            }
            context.setLineWidth(borderWidth*scale)
            context.stroke(rect)
            
            color.setFill()
            context.fill(rect.insetBy(dx: borderWidth, dy: borderWidth))
            
            guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
            UIGraphicsEndImageContext()
            
            self.init(cgImage: image.cgImage!, scale: scale, orientation: .up)
            return
        }
        
        var rect = CGRect()
        rect.size = size;
        
        if size.equalTo(CGSize.zero) {
            self.init()
            return nil
        }
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        color.setFill()
        
        rect.origin.x += borderWidth/2
        rect.origin.y += borderWidth/2
        rect.size.width -= borderWidth
        rect.size.height -= borderWidth
        
        if (borderWidth > 0)
        {
            context.setLineWidth(borderWidth)
        }
        
        if cornerInset.isRound {
            
            let path = UIBezierPath(roundedRect: rect, cornerRadius: cornerInset.topleft)
            path.lineWidth = borderWidth
            if let borderColor = borderColor {
                borderColor.setStroke()
                path.stroke()
            }
            path.fill()
            
        } else {
            
            context.beginPath()
            context.move(to: CGPoint(x: rect.minX, y: rect.midY))
            context.addArc(tangent1End: CGPoint(x: rect.minX, y: rect.midY), tangent2End: CGPoint(x: rect.minX, y: rect.midY), radius: cornerInset.topleft-borderWidth)
            context.addArc(tangent1End: CGPoint(x: rect.minX, y: rect.minY), tangent2End: CGPoint(x: rect.midX, y: rect.minY), radius: cornerInset.topleft-borderWidth)
            context.addArc(tangent1End: CGPoint(x: rect.maxX, y: rect.minY), tangent2End: CGPoint(x: rect.maxX, y: rect.midY), radius:  cornerInset.topright-borderWidth)
            context.addArc(tangent1End: CGPoint(x: rect.maxX, y: rect.maxY), tangent2End: CGPoint(x: rect.midX, y: rect.maxY), radius:  cornerInset.bottomright-borderWidth)
            context.addArc(tangent1End: CGPoint(x: rect.minX, y: rect.maxY), tangent2End: CGPoint(x: rect.minX, y: rect.midY), radius:  cornerInset.bottomleft-borderWidth)
            context.closePath()
            
            if let borderColor = borderColor {
                borderColor.setStroke()
                context.drawPath(using: .fillStroke)
            } else {
                context.drawPath(using: .fill)
            }
        }
        
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        
        image.cornerInset = cornerInset
        self.init(cgImage: image.cgImage!, scale: scale, orientation: .up)
    }
    
    public func resizableImage(_ horizonSpace: CGFloat = 0, verticalSpace: CGFloat = 0) -> UIImage {
        
        if self.resizingMode == UIImageResizingMode.stretch {
            return self
        } else if cornerInset != DGTCornerInset.zero {
            
            let edgeInset = UIEdgeInsetsMake(max(cornerInset.topleft, cornerInset.topright), max(cornerInset.topleft, cornerInset.bottomleft), max(cornerInset.bottomleft, cornerInset.bottomright), max(cornerInset.topright, cornerInset.bottomright));
            return self.resizableImage(withCapInsets: edgeInset)
        } else {
            let edgeInset = UIEdgeInsetsMake((size.height/2)+verticalSpace, (size.width/2)+horizonSpace, (size.height/2)+verticalSpace, (size.width/2)+horizonSpace)
            return self.resizableImage(withCapInsets: edgeInset)
        }
    }
    
    class func arrawImage(_ color: UIColor, direction: UIImageOrientation = .right, reduis: CGFloat = 7, lineWidth: CGFloat = 1, size: CGSize? = nil) -> UIImage? {
        
        let space: CGFloat = lineWidth/1.2
        let content: CGFloat = (reduis+space)*2
        let contentSize: CGSize
        if direction == .down || direction == .up {
            contentSize = CGSize(width: content, height: reduis+(space*2))
        } else {
            contentSize = CGSize(width: reduis+(space*2), height: content)
        }
        let x = contentSize.width-space
        let y = contentSize.height/2
        let R = reduis
        
        UIGraphicsBeginImageContextWithOptions(contentSize, false, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        color.setStroke()
        context.setLineWidth(lineWidth)
        
        switch direction {
        case .down:
            context.move(to: CGPoint(x: space, y: space))
            context.addLine(to:  CGPoint(x: contentSize.width/2, y: contentSize.height-space))
            context.addLine(to:  CGPoint(x: contentSize.width/2, y: contentSize.height-space))
            context.addLine(to:  CGPoint(x: x, y: space))
        case .up:
            context.move(to: CGPoint(x: space, y: contentSize.height-space))
            context.addLine(to:  CGPoint(x: contentSize.width/2, y: space))
            context.addLine(to:  CGPoint(x: x, y: contentSize.height-space))
        case .left:
            context.move(to: CGPoint(x: x, y: (y-R)))
            context.addLine(to:  CGPoint(x: (x-R), y: y))
            context.addLine(to:  CGPoint(x: x, y: (y+R)))
        default:
            context.move(to: CGPoint(x: (x-R), y: (y-R)))
            context.addLine(to:  CGPoint(x: x, y: y))
            context.addLine(to:  CGPoint(x: (x-R), y: y+R))
        }
        
        context.strokePath()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let size = size, !size.equalTo(CGSize.zero) {
            UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
            
            image?.draw(at: CGPoint(x: (size.width-contentSize.width)/2, y: (size.height-contentSize.height)/2))
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image
        }
        return image
    }
    
    public func tinted(_ color: UIColor) -> UIImage {
        return self.tinted([color])
    }
    
    public func tinted(_ colors: [UIColor], direction: DGTTintImageDirection = .vertical, locations: [CGFloat]? = nil) -> UIImage {
        if colors.isEmpty {
            return self
        }
        
        let gradientColors = colors.map {(color: UIColor!) -> AnyObject! in return color.cgColor as AnyObject! } as! [CGColor]
        
        let size = self.size
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()!
        if colors.count == 1 {
            context.translateBy(x: 0, y: size.height)
            context.scaleBy(x: 1.0, y: -1.0)
        }
        let rect = CGRect(origin: CGPoint.zero, size: size)
        context.clear(rect)
        
        context.setBlendMode(.normal)
        context.draw(self.cgImage!, in: rect)
        
        // Apply gradient
        context.clip(to: rect, mask: self.cgImage!)
        if (colors.count > 1) {
            // Create gradient
            let space = CGColorSpaceCreateDeviceRGB()
            let gradient = CGGradient(colorsSpace: space, colors: gradientColors as CFArray, locations: locations)!
            
            let clearColor = UIColor.clear
            clearColor.setFill()
            context.fill(rect)
            
            let horizontal = direction == DGTTintImageDirection.horizontal
            let startPoint = CGPoint(x: horizontal ? rect.minX : rect.midX, y: horizontal ? rect.midY : rect.minY)
            let endPoint = CGPoint(x: horizontal ? rect.maxX : rect.midX, y: horizontal ? rect.midY : rect.maxY)
            context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: [])
        } else {
            colors.first?.setFill()
            context.fill(rect)
        }
        
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return gradientImage
    }
    
    
    @objc(resizeImageToSize:)
    public func resize(_ size: CGSize) -> UIImage? {
        
        if size.equalTo(CGSize.zero) {
            return nil
        }
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        UIColor.clear.setFill()
        self.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    @objc(scaleImageToSize:)
    public func scaleImage(_ size: CGSize) -> UIImage? {
        
        if size.equalTo(CGSize.zero) {
            return nil
        }
        
        let imageSize: CGSize = self.size
        let width: CGFloat = imageSize.width
        let height: CGFloat = imageSize.height
        
        let targetWidth: CGFloat = size.width
        let targetHeight: CGFloat = size.height
        
        var scaledWidth: CGFloat = targetWidth
        var scaledHeight: CGFloat = targetHeight
        
        var thumbnailPoint = CGPoint.zero
        
        if (!imageSize.equalTo(size)) {
            
            let widthFactor = targetWidth / width
            let heightFactor = targetHeight / height
            let scaleFactor = widthFactor < heightFactor ? widthFactor : heightFactor
            
            scaledWidth  = width * scaleFactor
            scaledHeight = height * scaleFactor
            
            // make image center aligned
            if (widthFactor < heightFactor) {
                thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5
            } else if (widthFactor > heightFactor) {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5
            }
        }
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        
        let thumbnailRect = CGRect(origin: thumbnailPoint, size: CGSize(width: scaledWidth, height: scaledHeight))
        self.draw(in: thumbnailRect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    @objc(scaleImageToWidth:)
    public func scaleImageToWidth(_ width: CGFloat) -> UIImage? {
        return self.resize(CGSize(width: width, height: (self.size.height * width) / self.size.width))
    }
    
    @objc(scaleImageToHeight:)
    public func scaleImageToHeight(_ height: CGFloat) -> UIImage? {
        return self.resize(CGSize(width: (self.size.width * height) / self.size.height, height: height))
    }
    
    public func cropImageInRect(_ rect: CGRect) -> UIImage? {
        let size = self.size
        
        if CGRect(origin: CGPoint.zero, size: size).contains(rect) {
            var cropRect = rect
            cropRect.origin.x *= self.scale
            cropRect.origin.y *= self.scale
            cropRect.size.width *= self.scale
            cropRect.size.height *= self.scale
            return UIImage(cgImage: self.cgImage!.cropping(to: cropRect)!, scale: self.scale, orientation: .up)
        }
        return nil
    }
    
    public func imageAddingImage(_ image: UIImage?, offset: CGPoint = CGPoint.zero) -> UIImage {
        let size = self.size
        
        if image == nil {
            return self
        }
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        
        self.draw(at: CGPoint.zero)
        image!.draw(at: offset)
        let destImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return destImage
    }
    
    public func sprite(_ image: UIImage, range: NSRange? = nil, spriteSize size: CGSize) -> [UIImage]? {
        
        if size.equalTo(CGSize.zero) {
            return nil
        }
        
        let spriteSheet = image.cgImage!
        var images = [UIImage]()
        
        let width = CGFloat(spriteSheet.width)
        let height = CGFloat(spriteSheet.height)
        
        let maxI = Int(CGFloat(width) / size.width);
        
        var startI = 0, startJ = 0;
        
        // Extracting initial I & J values from range info
        //
        if let startPosition = range?.location, startPosition != 0 {
            for k in 0..<maxI {
                let d = k * maxI
                if d / startPosition == 1 {
                    startI = maxI - (d % startPosition)
                    break
                }
                else if (d/startPosition > 1) {
                    startI = startPosition
                    break
                }
                startJ += 1
            }
        }
        
        var positionX = CGFloat(startI) * size.width
        var positionY = CGFloat(startJ) * size.height
        var length = 0;
        var isReady = false;
        
        while (positionY < height) {
            while (positionX < width) {
                let rect = CGRect(x: positionX, y: positionY, width: size.width, height: size.height)
                
                if let sprite = spriteSheet.cropping(to: rect) {
                    images.append(UIImage(cgImage: sprite))
                    length += 1
                }
                
                if (length == range?.length) {
                    isReady = true
                    break
                }
                positionX += size.width;
            }
            
            if (isReady) {
                break
            }
            
            positionX = 0;
            positionY += size.height;
        }
        
        if images.isEmpty {
            return nil
        }
        return images
    }
    
    @objc(_cachedImageWithKey:)
    class func imageCache(_ key: String) -> UIImage? {
        if cache == nil {
            cache = NSCache()
        }
        if let obj = cache?.object(forKey: key as NSString) as? UIImage {
            return obj
        }
        return nil
    }
    
    @objc(_cacheImage:withKey:)
    class func cacheImage(_ image: UIImage, key: String) {
        if cache == nil {
            cache = NSCache()
        }
        cache?.setObject(image, forKey: key as NSString)
    }
    
    @objc(removeCacheImage:)
    class func removeCacheImage(_ key: String) {
        guard let cache = cache else {
            return
        }
        cache.removeObject(forKey: key as NSString)
    }
    
    @objc(_keyForImageWithDescription:)
    class func cacheKey(_ description: Dictionary<String, AnyObject>) -> String {
        var string: String = ""
        
        if let object = description[kDGTImageName] {
            string += "<\(kDGTImageName):\(object)>"
        }
        
        if let object = description[kDGTImageSize] {
            string += "<\(kDGTImageSize):\(object)>"
        }
        
        if let object = description[kDGTImageColors] {
            string += "<\(kDGTImageColors):\(object)>"
        }
        
        if let object = description[kDGTImageBorderColor] {
            string += "<\(kDGTImageBorderColor):\(object.hash)>"
        }
        
        if let object = description[kDGTImageBorderWidth] {
            string += "<\(kDGTImageBorderWidth):\(object)>"
        }
        
        if let object = description[kDGTImageTintColor] {
            string += "<\(kDGTImageTintColor):\(object.hash)>"
        }
        
        if let object = description[kDGTImageTintStyle] {
            string += "<\(kDGTImageTintStyle):\(object)>"
        }
        
        if let object = description[kDGTImageCornerInset] {
            string += "<\(kDGTImageCornerInset):\(object)>"
        }
        return string
    }
}

extension UIView {
    
    /**
     *  The method use for get CGPoint of view in center coordination
     *
     *  @return CGPoint of center in this view
     */
    func centerPointOfView() -> CGPoint {
        return CGPoint(x: self.bounds.width/2, y: self.bounds.height/2);
    }
    
    @objc(firstAvailableUIViewController)
    public func firstAvailableUIViewController() -> UIViewController? {
        let nextResponder = self.next
        if nextResponder is UIViewController {
            return nextResponder as? UIViewController
        } else if nextResponder is UIView {
            let view = nextResponder as? UIView
            return view!.firstAvailableUIViewController()
        } else {
            return nil
        }
    }
    
    @objc(firstSuperViewForClass:)
    public func firstSuperView(_ viewClass: AnyClass) -> AnyObject? {
        
        // iterate up the view hierarchy to find the table containing this view
        var view = self.superview
        while (view != nil) {
            if view!.isKind(of:viewClass) {
                return view
            }
            view = view!.superview
        }
        return nil // this view is not within a view
    }
    
    @objc(firstSubviewWithClass:)
    public func firstSubview(_ viewClass: AnyClass) -> AnyObject? {
        return findSubviews(viewClass)?.first
    }
    
    @objc(findSubviews:)
    public func findSubviews(_ viewClass: AnyClass) -> [AnyObject]? {
        
        let subViews = subviews.filter{ $0.isKind(of: viewClass) }
        if !subViews.isEmpty {
            return subViews
        }
        for subView in subviews {
            if let inSubViews = subView.findSubviews(viewClass) {
                return inSubViews
            }
        }
        return nil
    }
    
    func takeSnapshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale);
        
        if responds(to: #selector(UIView.drawHierarchy(in:afterScreenUpdates:))) {
            drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        } else {
            layer.render(in: UIGraphicsGetCurrentContext()!)
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return image
    }
}

private var dismissAlertView: ((UIAlertView, Int) -> Void)?

extension UIAlertView: UIAlertViewDelegate {
    
    class func show(title: String?, description: String?, cancelButtonTitle: String?, dismiss: ((UIAlertView, Int) -> Void)? = nil) {
        let alert = UIAlertView(title: title, message: description, delegate: nil, cancelButtonTitle: cancelButtonTitle)
        if let dismiss = dismiss {
            alert.delegate = alert
            dismissAlertView = dismiss
        }
        alert.show()
    }
    
    class func show(error: NSError, dismiss: ((UIAlertView, Int) -> Void)? = nil) {
        let alert = UIAlertView(title: "", message: error.localizedDescription, delegate: nil, cancelButtonTitle: "Ok")
        if let dismiss = dismiss {
            alert.delegate = alert
            dismissAlertView = dismiss
        }
        alert.show()
    }
    
    public func show(dismiss: ((UIAlertView, Int) -> Void)?) {
        if let dismiss = dismiss {
            delegate = self
            dismissAlertView = dismiss
        }
        show()
    }
    
    @objc(isVisible)
    public class func isVisible() -> Bool {
        for object : AnyObject in UIApplication.shared.windows {
            let window: UIWindow = object as! UIWindow
            for inObject: AnyObject in window.subviews {
                if inObject is UIAlertView {
                    return true
                }
            }
        }
        return false
    }
    
    public func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if let dismissCallBack = dismissAlertView {
            dismissCallBack(alertView, buttonIndex)
            dismissAlertView = nil
        }
    }
}

private var dismissActionSheet: ((UIActionSheet, Int) -> Void)?

extension UIActionSheet: UIActionSheetDelegate {
    
    public func showInView(_ _view: UIView!, dismiss: ((UIActionSheet, Int) -> Void)?) {
        if let dismiss = dismiss {
            delegate = self
            dismissActionSheet = dismiss
        }
        show(in: _view)
    }
    
    public func actionSheet(_ _actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        if let dismissCallBack = dismissActionSheet {
            dismissCallBack(_actionSheet, buttonIndex)
        }
    }
}

extension UIFont {
    
    public class func preferredFontForTextStyle(_ style: String, scale: CGFloat) -> UIFont {
        let descriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle(rawValue: style))
        let pointSize = descriptor.pointSize * scale
        return UIFont(descriptor: descriptor, size: pointSize)
    }
}

extension UITableView {
    
    fileprivate struct AssociatedKeys {
        static var cellIdentifiers = "tableViewCellIdentifiers"
        static var editedCompletion = "editedCompletion"
    }
    
    class ClosureWrapper {
        var closure: (() -> Void)?
        
        init(_ closure: (() -> Void)?) {
            self.closure = closure
        }
    }
    
    //this lets us check to see if the item is supposed to be displayed or not
    fileprivate var cellIdentifiers: [String: UITableViewCell] {
        get {
            guard let tableViewCells = objc_getAssociatedObject(self, &AssociatedKeys.cellIdentifiers) as? [String: UITableViewCell] else {
                return [String: UITableViewCell]()
            }
            return tableViewCells
        }
        
        set(value) {
            objc_setAssociatedObject(self,&AssociatedKeys.cellIdentifiers, value,objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    fileprivate var editedCompletion: (() -> Void)? {
        get {
            guard let completion = objc_getAssociatedObject(self, &AssociatedKeys.editedCompletion) as? ClosureWrapper else {
                return nil
            }
            return completion.closure
        }
        
        set(value) {
            objc_setAssociatedObject(self,&AssociatedKeys.editedCompletion, ClosureWrapper(value),objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    public func performBatchUpdatess(_ update: (() -> Void), completion: (() -> Void)? = nil) {
        editedCompletion = completion
        CATransaction.begin()
        beginUpdates()
        CATransaction.setCompletionBlock { [weak self] in
            self?.editedCompletion?()
            self?.editedCompletion = nil
        }
        update()
        endUpdates()
        CATransaction.commit()
    }
    
    public func dynamicRowHeight(_ className: AnyClass, updateContent: ((_ cell: UITableViewCell) -> Void)? = nil) -> CGFloat {
        let cell = cellReuseableForDynamicHeight(className)
        
        if let updateContent = updateContent {
            updateContent(cell)
        }
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        let targetSize = CGSize(width: bounds.width, height: UILayoutFittingCompressedSize.height)
        if #available(iOS 8.0, *) {
            return cell.contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: 1000, verticalFittingPriority: 250).height
        } else {
            // Fallback on earlier versions
            return cell.contentView.systemLayoutSizeFitting(targetSize).height
        }
    }
    
    public func cellReuseableForDynamicHeight(_ className: AnyClass) -> UITableViewCell {
        let identifier = (className as! UITableViewCell.Type).reuseIdentifier()
        let cell: UITableViewCell
        if let incell = cellIdentifiers[identifier] {
            cell = incell
            cell.prepareForReuse()
        } else {
            cell = dequeueReusableCell(withIdentifier: identifier)!
            cellIdentifiers[identifier] = cell
        }
        return cell
    }
    
    public func clearTableViewCellForDynamicHeight() {
        cellIdentifiers.removeAll()
    }
    
    public func removeTableViewCellForDynamicHeight(_ identifier: String) {
        cellIdentifiers.removeValue(forKey: identifier)
    }
}

@objc protocol DGTReuseableView {
    
    static func reuseIdentifier() -> String
    static func rowHeight() -> CGFloat
}

extension UITableViewCell: DGTReuseableView {
    
    public class func reuseIdentifier() -> String {
        let className = NSStringFromClass(self)
        let component = className.components(separatedBy: ".")
        if component.count > 1 {
            return component.last! + "Identifier"
        }
        return className + "Identifier"
    }
    
    public class func rowHeight() -> CGFloat {
        return 44.0
    }
}

extension UICollectionReusableView: DGTReuseableView {
    
    public class func reuseIdentifier() -> String {
        let className = NSStringFromClass(self)
        let component = className.components(separatedBy: ".")
        if component.count > 1 {
            return component.last! + "Identifier"
        }
        return className + "Identifier"
    }
    
    public class func rowHeight() -> CGFloat {
        return 44.0
    }
}

extension UITableViewHeaderFooterView: DGTReuseableView {
    
    public class func reuseIdentifier() -> String {
        let className = NSStringFromClass(self)
        let component = className.components(separatedBy: ".")
        if component.count > 1 {
            return component.last! + "Identifier"
        }
        return className + "Identifier"
    }
    
    public class func rowHeight() -> CGFloat {
        return 44.0
    }
}

extension UINavigationController {
    
    func pushViewController(_ viewController: UIViewController, animated: Bool, completion: @escaping (Void) -> Void) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
}

var DGTFontWeightUltraLight: CGFloat {
    if #available(iOS 8.2, *) {
        return UIFontWeightUltraLight
    }
    return 0.1
}
var DGTFontWeightLight: CGFloat {
    if #available(iOS 8.2, *) {
        return UIFontWeightLight
    }
    return 0.2
}
var DGTFontWeightThin: CGFloat {
    if #available(iOS 8.2, *) {
        return UIFontWeightThin
    }
    return 0.3
}
var DGTFontWeightRegular: CGFloat {
    if #available(iOS 8.2, *) {
        return UIFontWeightRegular
    }
    return 0.4
}
var DGTFontWeightMedium: CGFloat {
    if #available(iOS 8.2, *) {
        return UIFontWeightMedium
    }
    return 0.5
}
var DGTFontWeightSemibold: CGFloat {
    if #available(iOS 8.2, *) {
        return UIFontWeightSemibold
    }
    return 0.6
}
var DGTFontWeightBold: CGFloat {
    if #available(iOS 8.2, *) {
        return UIFontWeightBold
    }
    return 0.7
}
var DGTFontWeightHeavy: CGFloat {
    if #available(iOS 8.2, *) {
        return UIFontWeightHeavy
    }
    return 0.8
}
var DGTFontWeightBlack: CGFloat {
    if #available(iOS 8.2, *) {
        return UIFontWeightBlack
    }
    return 0.9
}

private var _familyName: String? = nil
private var _fontAttibutes: [CGFloat: String] = [:]
private var _fontFamilyAttibutes: [CGFloat: String] = [:]
extension UIFont {
    
    @discardableResult
    public class func registerFontFamily(_ familyName: String) -> Bool {
        let fonts = UIFont.fontNames(forFamilyName: familyName)
        if !fonts.isEmpty {
            _familyName = familyName
            _fontFamilyAttibutes.removeAll()
            return true
        }
        return false
    }
    
    public class func registerFont(_ fontName: String, forWeight weight: CGFloat) -> Bool {
        if let _ = UIFont(name: fontName, size: systemFontSize) {
            _fontAttibutes[weight] = fontName
        }
        return false
    }
    
    public class func customFont(_ fontSize: CGFloat, weight: CGFloat = DGTFontWeightRegular) -> UIFont {
        if let name = _fontAttibutes[weight], let font = UIFont(name: name, size: fontSize) {
            return font
        }
        if let name = _fontFamilyAttibutes[weight], let font = UIFont(name: name, size: fontSize) {
            return font
        }
        
        let filter: [String]
        switch weight {
        case DGTFontWeightUltraLight: filter = ["UlLi", "UltraLight"]
        case DGTFontWeightThin: filter = ["Thin"]
        case DGTFontWeightLight: filter = ["Li", "Light"]
        case DGTFontWeightRegular: filter = ["Regular", "Reg"]
        case DGTFontWeightMedium: filter = ["Med", "Medium"]
        case DGTFontWeightSemibold: filter = ["SemiBd", "Semibold"]
        case DGTFontWeightBold: filter = ["Bd", "Bold"]
        case DGTFontWeightHeavy: filter = ["Heavy"]
        case DGTFontWeightBlack: filter = ["Blk", "Black"]
        default: filter = []
        }
        
        let listFont: [String]
        if let familyName = _familyName {
            listFont = UIFont.fontNames(forFamilyName: familyName)
        } else {
            listFont = UIFont.fontNames(forFamilyName: "Helvetica Neue")
        }
        
        if let fontName = listFont.first(where: { name in
            var match = false
            for value in filter {
                match = name.contains(value)
                if match { break }
            }
            return match
        }) {
            _fontFamilyAttibutes[weight] = fontName
            return UIFont(name: fontName, size: fontSize)!
        }
        
        if #available(iOS 8.2, *) {
            return UIFont.systemFont(ofSize: fontSize, weight: weight)
        } else {
            if weight == DGTFontWeightBold {
                return UIFont.boldSystemFont(ofSize: fontSize)
            }
            return UIFont.systemFont(ofSize: fontSize)
        }
    }
}

extension UINib {
    
    convenience init(viewClass: AnyClass) {
        self.init(nibName: StringFromClass(viewClass), bundle: Bundle(for: viewClass))
    }
}

extension UITraitEnvironment {
    
    var isRegularHorizontalSizeClass: Bool {
        if #available(iOS 8.0, *) {
            return traitCollection.horizontalSizeClass == .regular
        }
        return DeviceType.IS_IPAD
    }
    
    var isRegularVerticalSizeClass: Bool {
        if #available(iOS 8.0, *) {
            return traitCollection.verticalSizeClass == .regular
        }
        return DeviceType.IS_IPAD
    }
}
