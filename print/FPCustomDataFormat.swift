//
//  FPCustomDataFormat.swift
//  print
//
//  Created by Prakit Kongkaew on 3/21/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

import Foundation
import UIKit

func DLog(_ format: String, _ args: CVarArg? = nil, _ path: String = #file, _ function: String = #function, _ line: Int = #line) {
    
    if DEBUG_BUILD.boolValue {
        let file = path.components(separatedBy: "/").last?.components(separatedBy: ".").first
        let log: String
        if let args = args {
            log = String(format: format, args)
        } else {
            log = format
        }
        
        if let file = file {
            print("[\(file):\(line)] \(function): \(log)")
        } else {
            print(format)
        }
    }
}

func LocalizeString(_ string: String) -> String {
    return NSLocalizedString(string, comment: "")
}

func StringFromClass(_ objectClass: AnyClass) -> String {
    let className = NSStringFromClass(objectClass)
    let component = className.components(separatedBy: ".")
    if component.count > 1 {
        return component.last!
    }
    return className
}

func < (left: NSObject, right: NSObject) -> Bool {
    if left.responds(to: #selector(NSNumber.compare(_:))) {
        let result = left.perform(#selector(NSNumber.compare(_:)), with: right)
        let resultInt = unsafeBitCast(result, to: Int.self)
        return ComparisonResult(rawValue: resultInt)! == .orderedAscending
    }
    return false
}

func > (left: NSObject, right: NSObject) -> Bool {
    if left.responds(to: #selector(NSNumber.compare(_:))) {
        let result = left.perform(#selector(NSNumber.compare(_:)), with: right)
        let resultInt = unsafeBitCast(result, to: Int.self)
        return ComparisonResult(rawValue: resultInt)! == .orderedDescending
    }
    return false
}

func == (left: NSObject, right: NSObject) -> Bool {
    if left.responds(to: #selector(NSNumber.compare(_:))) {
        let result = left.perform(#selector(NSNumber.compare(_:)), with: right)
        let resultInt = unsafeBitCast(result, to: Int.self)
        return ComparisonResult(rawValue: resultInt)! == .orderedSame
    }
    return left.isEqual(right)
}

func <= (left: NSObject, right: NSObject) -> Bool {
    if left.responds(to: #selector(NSNumber.compare(_:))) {
        let result = left.perform(#selector(NSNumber.compare(_:)), with: right)
        let resultInt = unsafeBitCast(result, to: Int.self)
        return ComparisonResult(rawValue: resultInt)! != .orderedDescending
    }
    return false
}

func >= (left: NSObject, right: NSObject) -> Bool {
    if left.responds(to: #selector(NSNumber.compare(_:))) {
        let result = left.perform(#selector(NSNumber.compare(_:)), with: right)
        let resultInt = unsafeBitCast(result, to: Int.self)
        return ComparisonResult(rawValue: resultInt)! != .orderedAscending
    }
    return false
}

func != (left: NSObject, right: NSObject) -> Bool {
    if left.responds(to: #selector(NSNumber.compare(_:))) {
        let result = left.perform(#selector(NSNumber.compare(_:)), with: right)
        let resultInt = unsafeBitCast(result, to: Int.self)
        return ComparisonResult(rawValue: resultInt)! != .orderedSame
    }
    return !left.isEqual(right)
}

extension NSRange: Equatable {}

func < (left: NSRange, right: NSRange) -> Bool {
    return left.location < right.location
}

func > (left: NSRange, right: NSRange) -> Bool {
    return left.location > right.location
}

public func == (left: NSRange, right: NSRange) -> Bool {
    return left.location == right.location && left.length == right.length
}

func += <KeyType, ValueType>(left: inout Dictionary<KeyType, ValueType>, right: Dictionary<KeyType, ValueType>) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}
//
//class EnumGenerator<Enum : Hashable> : IteratorProtocol {
//    var rawEnum = 0
//    var done = false
//
//    func next() -> Enum? {
//        if done { return nil }
//
//        let enumCase = withUnsafePointer(to: &rawEnum) { UnsafePointer<Enum>($0).pointee }
//        if enumCase.hashValue == rawEnum {
//            rawEnum += 1
//            return enumCase
//        } else {
//            done = true
//            return nil
//        }
//    }
//}
//
//class SingleEnumGenerator<Enum : Hashable> : EnumGenerator<Enum> {
//    override func next() -> Enum? {
//        return done ? nil : { done = true; return unsafeBitCast((), Enum.self) }()
//    }
//}
//
//protocol EnumCollection : Hashable {}
//extension EnumCollection {
//    static func cases() -> EnumSequence<Self> {
//        return EnumSequence()
//    }
//}
//
//struct EnumSequence<Enum : Hashable> : SequenceType {
//    func generate() -> EnumGenerator<Enum> {
//        switch sizeof(Enum) {
//        case 0: return SingleEnumGenerator()
//        default: return EnumGenerator()
//        }
//    }
//}

extension NSValue {
    
    convenience init(cornerInset: DGTCornerInset) {
        let rect = CGRect(x: cornerInset.topleft, y: cornerInset.topright, width: cornerInset.bottomleft, height: cornerInset.bottomright)
        self.init(cgRect: rect)
    }
    
    func dgtCornerInsetValue() -> DGTCornerInset {
        let rect = self.cgRectValue
        return DGTCornerInset(topleft: rect.origin.x, topright: rect.origin.y, bottomleft: rect.width, bottomright: rect.height)
    }
}

extension DateFormatter {
    
    public class var formatter : DateFormatter {
        struct Static {
            static let instance : DateFormatter = DateFormatter()
        }
        return Static.instance
    }
    
    public class func date(format: String, date: String, locale: Locale? = Locale.usLocale()) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = locale
        return dateFormatter.date(from: date)
    }
    
    public class func string(format: String, date: Date, locale: Locale) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = locale
        return dateFormatter.string(from:date)
    }
}

extension NumberFormatter {
    
    public class var formatter: NumberFormatter {
        struct Static {
            static let instance = NumberFormatter()
        }
        return Static.instance
    }
    
    public class func decimal(_ number: Int) -> String {
        return NumberFormatter.formatter.string(from: NSNumber(value: number))!
    }
    
    public class func currency(_ number: Int, locale: Locale = Locale.usLocale(), symbol: String! = "") -> String! {
        let formatter = NumberFormatter.formatter
        formatter.currencySymbol = symbol
        formatter.numberStyle = .currency
        return formatter.string(from: NSNumber(value: number))
    }
}

extension String {
    
    public func size(font: UIFont) -> CGSize {
        let text: NSString = NSString(string: self)
        return text.size(attributes: [NSFontAttributeName: font])
    }
    
    public func size(font: UIFont, constrained size: CGSize, lineBreakMode: NSLineBreakMode? = nil) -> CGSize {
        let text: NSString = NSString(string: self)
        
        let attibutes: [String: AnyObject]
        if let lineBreakMode = lineBreakMode {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineBreakMode = lineBreakMode
            attibutes = [NSFontAttributeName: font, NSParagraphStyleAttributeName: paragraphStyle]
        } else {
            attibutes = [NSFontAttributeName: font]
        }
        return text.boundingRect(with: size, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: attibutes, context: nil).size
    }
    
    public func size(font: UIFont, width: CGFloat, lineBreakMode: NSLineBreakMode = NSLineBreakMode.byWordWrapping) -> CGSize {
        return self.size(font: font, constrained: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), lineBreakMode: lineBreakMode)
    }
    
    func replaceCharacters(in range:Range<Int>, string: String) -> String {
        let result = NSMutableString(string: self)
        result.replaceCharacters(in: NSRange(range), with: string)
        return result as String
    }
    
    var length:Int {
        return (self as NSString).length
    }
}

extension Sequence {
    
    func filterIndexs(_ includeElement: (Self.Iterator.Element) -> Bool) -> [Int] {
        var indexs = [Int]()
        for (i, value) in self.enumerated() {
            if includeElement(value) {
                indexs.append(i)
            }
        }
        return indexs
    }
    
    func firstIndex(where predicate: (Self.Iterator.Element) -> Bool) -> Int? {
        
        var index: Int?
        for (i, value) in self.enumerated() {
            if predicate(value) {
                index = i;break
            }
        }
        return index
    }
    
    func firstObjectAndIndex(where predicate: (Self.Iterator.Element) -> Bool) -> (Int, Self.Iterator.Element)? {
        
        var indexObject: (Int, Self.Iterator.Element)?
        for (i, value) in self.enumerated() {
            if predicate(value) {
                indexObject = (i, value);break
            }
        }
        return indexObject
    }
    
}

extension Double {
    func toString(_ decimal: Int = 0) -> String {
        return String(format: "%.\(decimal)f",self)
    }
}

extension Int
{
    static func random(_ range: Range<Int> ) -> Int
    {
        var offset = 0
        
        if range.lowerBound < 0   // allow negative ranges
        {
            offset = abs(range.lowerBound)
        }
        
        let mini = UInt32(range.lowerBound + offset)
        let maxi = UInt32(range.upperBound   + offset)
        
        return Int(mini + arc4random_uniform(maxi - mini)) - offset
    }
}

extension Date {
    
    public static func deleteTime() -> Date {
        return Date().deleteTime()
    }
    
    public func deleteTime() -> Date {
        let calendar = Calendar.current
        let component = calendar.dateComponents(Calendar.Component.allDay, from: self)
        return calendar.date(from: component)!
    }
    
    public func set(_ unit: Set<Calendar.Component>, value: Int) -> Date {
        let calendar = Calendar.current
        let calendarUnit = self.checkDateComponent(unit)
        var component = calendar.dateComponents(calendarUnit, from: self)
        
        if unit.contains(.era) {
            component.era = value
        } ; if unit.contains(.year) {
            component.year = value
        } ; if unit.contains(.month) {
            component.month = value
        } ; if unit.contains(.day) {
            component.day = value
        } ; if unit.contains(.hour) {
            component.hour = value
        } ; if unit.contains(.minute) {
            component.minute = value
        } ; if unit.contains(.second) {
            component.second = value
        } ; if unit.contains(.weekday) {
            component.weekday = value
        } ; if unit.contains(.weekdayOrdinal) {
            component.weekdayOrdinal = value
        } ; if unit.contains(.quarter) {
            component.quarter = value
        } ; if unit.contains(.weekOfMonth) {
            component.weekOfMonth = value
        } ; if unit.contains(.weekOfYear) {
            component.weekOfYear = value
        } ; if unit.contains(.yearForWeekOfYear) {
            component.yearForWeekOfYear = value
        }
        return calendar.date(from: component)!
    }
    
    public static func add(_ unit: Set<Calendar.Component>, value: Int) -> Date {
        return Date().add(unit, value: value)
    }
    
    public func add(_ unit: Set<Calendar.Component>, value: Int) -> Date {
        let calendar = Calendar.current
        let calendarUnit = self.checkDateComponent(unit)
        
        var component = calendar.dateComponents(calendarUnit, from: self)
        
        let uInt = Int(fabs(Double(value)))
        if unit.contains(.era) {
            component.era = value < 0 ? component.era! - uInt : component.era! + value
        } ; if unit.contains(.year) {
            component.year = value < 0 ? component.year! - uInt : component.year! + value
        } ; if unit.contains(.month) {
            component.month = value < 0 ? component.month! - uInt : component.month! + value
        } ; if unit.contains(.day) {
            component.day = value < 0 ? component.day! - uInt : component.day! + value
        } ; if unit.contains(.hour) {
            component.hour = value < 0 ? component.hour! - uInt : component.hour! + value
        } ; if unit.contains(.minute) {
            component.minute = value < 0 ? component.minute! - uInt : component.minute! + value
        } ; if unit.contains(.second) {
            component.second = value < 0 ? component.second! - uInt : component.second! + value
        } ; if unit.contains(.weekday) {
            component.weekday = value < 0 ? component.weekday! - uInt : component.weekday! + value
        } ; if unit.contains(.weekdayOrdinal) {
            component.weekdayOrdinal = value < 0 ? component.weekdayOrdinal! - uInt : component.weekdayOrdinal! + value
        } ; if unit.contains(.quarter) {
            component.quarter = value < 0 ? component.quarter! - uInt : component.quarter! + value
        } ; if unit.contains(.weekOfMonth) {
            component.weekOfMonth = value < 0 ? component.weekOfMonth! - uInt : component.weekOfMonth! + value
        } ; if unit.contains(.weekOfYear) {
            component.weekOfYear = value < 0 ? component.weekOfYear! - uInt : component.weekOfYear! + value
        } ; if unit.contains(.yearForWeekOfYear) {
            component.yearForWeekOfYear = value < 0 ? component.yearForWeekOfYear! - uInt : component.yearForWeekOfYear! + value
        }
        return calendar.date(from: component)!
    }
    
    fileprivate func checkDateComponent(_ unit: Set<Calendar.Component>) -> Set<Calendar.Component> {
        var calendarUnit = Calendar.Component.allDayTime
        if calendarUnit.contains(.weekday) {
            calendarUnit = [.year, .month, .hour, .minute, .second, .weekday]
        }
        if calendarUnit.contains(.quarter) {
            calendarUnit = calendarUnit.union([.year, .quarter, .hour, .minute, .second])
        }
        return calendarUnit
    }
}

extension Calendar {
    
    public func valueComponent(of unit: Calendar.Component, date: Date) -> Int {
        if #available(iOS 8.0, *) {
            return self.component(unit, from: date)
        }
        
        let component = self.dateComponents([.era, .year, .month, .day, .hour, .minute, .second, .weekday, .weekdayOrdinal, .quarter, .weekOfMonth, .weekOfYear, .yearForWeekOfYear], from: date)
        
        if unit == .era {
            return component.era!
        } ; if unit == .year {
            return component.year!
        } ; if unit == .month {
            return component.month!
        } ; if unit == .day {
            return component.day!
        } ; if unit == .hour {
            return component.hour!
        } ; if unit == .minute {
            return component.minute!
        } ; if unit == .second {
            return component.second!
        } ; if unit == .weekday {
            return component.weekday!
        } ; if unit == .weekdayOrdinal {
            return component.weekdayOrdinal!
        } ; if unit == .quarter {
            return component.quarter!
        } ; if unit == .weekOfMonth {
            return component.weekOfMonth!
        } ; if unit == .weekOfYear {
            return component.weekOfYear!
        } ; if unit == .yearForWeekOfYear {
            return component.yearForWeekOfYear!
        }
        return 0
    }
    
}

extension Calendar.Component {
    public static var allDayTime: Set<Calendar.Component> {
        return self.allDay.union(self.allTime)
    }
    
    public static var allDay: Set<Calendar.Component> {
        return [.year, .month, .day]
    }
    
    public static var allTime: Set<Calendar.Component> {
        return [.hour, .minute, .second]
    }
}

extension Locale {
    
    public static func usLocale() -> Locale {
        return Locale(identifier: "en_US")
    }
    
    public static func thLocale() -> Locale {
        return Locale(identifier: "th_TH")
    }
}

extension Data {
    
    /// Create hexadecimal string representation of NSData object.
    ///
    /// :returns: String representation of this NSData object.
    
    func hexadecimalString() -> String {
        let string = NSMutableString(capacity: count * 2)
        var byte: UInt8?
        
        for i in 0 ..< count {
            copyBytes(to: &byte!, from: Range<Data.Index>(uncheckedBounds: (lower: i, upper: 1)))
            string.appendFormat("%02x", byte!)
        }
        
        return string as String
    }
}

extension Dictionary {
    
    func stringFromQueryParameters() -> String {
        var parts: [String] = []
        for (name, value) in self {
            guard let paramName = (name as? String)?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
                continue
            }
            
            let paramValue: String
            if let value = (value as? String)?.addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed) {
                paramValue = value
            } else {
                paramValue = String(describing: value)
            }
            parts.append(paramName + "=" + paramValue)
        }
        return parts.joined(separator: "&")
    }
}

@objc protocol DGTCustomObject {
    init?(dict: [String: Any])
}

extension String {
    
    // Hexadecimal to Decimal
    var hexaToDecimal: Int {
        var decimal = 0
        if self == "" { return 0 }
        for index in self.characters {
            for char in 0..<16 {
                let charecter = Array("0123456789abcdef".characters)[char]
                if index == charecter {
                    decimal = decimal * 16 + char
                }
            }
        }
        return self.characters.first! == "-" ? decimal * -1 : decimal
    }
    
    
    // Decimal to Hexadecimal
    var decimalToHexa: String {
        return String(Int(self)!, radix: 16)
    }
    // Decimal to Binary
    var decimalToBinary: String {
        return String(Int(self)!, radix: 2)
    }
}

extension Int {
    // Decimal to Binary
    var toBinary: String {
        return String(self, radix: 2)
    }
    // Decimal to Hexadecimal
    var toHexa: String {
        return String(self, radix: 16)
    }
}

extension Collection {
    public subscript(safe safeIndex: Index) -> Self.Iterator.Element? {
        return self.distance(from: safeIndex, to: endIndex) > 0 && self.distance(from: startIndex, to: safeIndex) >= 0 ? self[safeIndex] : nil
    }
}

extension UIColor {
    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        return (r,g,b,a)
    }
}

extension NSError {
    
    class func unknowError() -> NSError {
        return NSError(domain: "com.digitopolis.error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Unknow Error"])
    }
    
    convenience init(code: Int, description: String) {
        self.init(domain: "com.digitopolis.error", code: code, userInfo: [NSLocalizedDescriptionKey: description])
    }
}

extension NSObject {
    
    func className() -> String {
        let className = NSStringFromClass(self.classForCoder)
        let component = className.components(separatedBy: ".")
        if component.count > 1 {
            return component.last!
        }
        return className
    }
}
extension Double
{
    func truncate(places : Int)-> Double
    {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}
