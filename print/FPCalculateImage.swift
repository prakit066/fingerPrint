//
//  FPCalculateImage.swift
//  print
//
//  Created by Prakit Kongkaew on 3/27/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

public struct Pixel {
    var value: UInt32
    var red: UInt8 {
        get { return UInt8(value & 0xFF) }
        set { value = UInt32(newValue) | (value & 0xFFFFFF00) }
    }
    var green: UInt8 {
        get { return UInt8((value >> 8) & 0xFF) }
        set { value = (UInt32(newValue) << 8) | (value & 0xFFFF00FF) }
    }
    var blue: UInt8 {
        get { return UInt8((value >> 16) & 0xFF) }
        set { value = (UInt32(newValue) << 16) | (value & 0xFF00FFFF) }
    }
    var alpha: UInt8 {
        get { return UInt8((value >> 24) & 0xFF) }
        set { value = (UInt32(newValue) << 24) | (value & 0x00FFFFFF) }
    }
}
class RGBA {
    var pixels: UnsafeMutableBufferPointer<Pixel>
    
    var width: Int
    var height: Int
    var contrastValue : Double
    init?(image: UIImage) {
        guard let cgImage = image.cgImage else { return nil } // 1
        contrastValue = 0
        width = Int(image.size.width)
        height = Int(image.size.height)
        let bitsPerComponent = 8 // 2
        
        let bytesPerPixel = 4
        let bytesPerRow = width * bytesPerPixel
        let imageData = UnsafeMutablePointer<Pixel>.allocate(capacity: width * height)
        let colorSpace = CGColorSpaceCreateDeviceRGB() // 3
        
        var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Big.rawValue
        bitmapInfo |= CGImageAlphaInfo.premultipliedLast.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
        guard let imageContext = CGContext(data: imageData, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo) else { return nil }
        imageContext.draw(cgImage, in: CGRect(origin: CGPoint.zero, size: image.size))
        pixels = UnsafeMutableBufferPointer<Pixel>(start: imageData, count: width * height)
        
    }
    deinit {
    
    }
    func toUIImage() -> UIImage? {
        let bitsPerComponent = 8 // 1
        
        let bytesPerPixel = 4
        let bytesPerRow = width * bytesPerPixel
        let colorSpace = CGColorSpaceCreateDeviceRGB() // 2
        
        var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Big.rawValue
        bitmapInfo |= CGImageAlphaInfo.premultipliedLast.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
        let imageContext = CGContext(data: pixels.baseAddress, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo, releaseCallback: nil, releaseInfo: nil)
        guard let cgImage = imageContext!.makeImage() else {return nil} // 3
        
        let image = UIImage(cgImage: cgImage)
        return image
    }
    func toGrayScale() {
        var avg = 0.0
        for y in 0..<height {
            var avgrow = 0.0
            for x in 0..<width {
                let index = y * width + x
                var pixel = pixels[index]
                var mid1  = (0.2989 * Float(pixel.red))  + (0.5870 * Float(pixel.green)) + (0.1140 * Float(pixel.blue))
                var mid : UInt8 = UInt8(mid1)
                pixel.red = mid
                pixel.green = mid
                pixel.blue = mid
                
                pixels[index] = pixel
                avgrow = avgrow + Double(mid1)
            }
            avg = avg + (avgrow / Double(width))
        }
        avg = avg / Double(height)
        threshold( UInt8(avg))
    }
    func threshold(_ avg : UInt8 ) {
        
        for y in 0..<height {
            
            for x in 0..<width {
                let index = y * width + x
                var pixel = pixels[index]
                let x = Int(pixel.red)
                
                let  mid = x > Int(avg) ? findMin(a: 254, b: (x  + 20) ): findMax(a: 10, b: (x - 20))
                pixel.red = mid
                pixel.green = mid
                pixel.blue = mid
                
                pixels[index] = pixel
            }
        }

    }
    func contrast(image: RGBA) -> RGBA {
        var totalRed = 0
        var totalGreen = 0
        var totalBlue = 0
        
        for y in 0..<height {
            for x in 0..<width {
                let index = y * width + x
                let pixel = pixels[index]
                totalRed += Int(pixel.red)
                totalGreen += Int(pixel.green)
                totalBlue += Int(pixel.blue)
            }
        }
        
        let pixelCount = width * height
        let avgRed = totalRed / pixelCount
        let avgGreen = totalGreen / pixelCount
        let avgBlue = totalBlue / pixelCount
        for y in 0..<image.height {
            for x in 0..<image.width {
                let index = y * image.width + x
                var pixel = image.pixels[index]
                let redDelta = Int(pixel.red) - avgRed
                let greenDelta = Int(pixel.green) - avgGreen
                let blueDelta = Int(pixel.blue) - avgBlue
                pixel.red = UInt8(max(min(255, avgRed + 3 * redDelta), 0))
                pixel.green = UInt8(max(min(255, avgGreen + 3 * greenDelta), 0))
                pixel.blue = UInt8(max(min(255, avgBlue + 3 * blueDelta), 0))
                image.pixels[index] = pixel
            }
        }
        return image
    }
    func adjust( c: Double , bri : Double) {
        
        
        let factor =  (259.0*(c + 255.0)) / (255.0*(259.0 - c))
        print(factor , height , width)
        
        for y in 0..<height {
            for x in 0..<width {
                let index = y * width + x
                var pixel = pixels[index]
                
                let r = (factor * (Double(pixel.red) - 128.0) + 128.0) + bri
                let g = (factor * (Double(pixel.green) - 128.0) + 128.0) + bri
                let b = (factor * (Double(pixel.blue) - 128.0) + 128.0) + bri
                pixel.red = UInt8( max(min(255.0,r.truncate(places: 0)),0))
                pixel.green = UInt8( max(min(255.0,g.truncate(places: 0)),0))
                pixel.blue = UInt8( max(min(255.0,b.truncate(places: 0)),0))
                pixels[index] = pixel
            }
        }
        print("completed")
    
    }
    func findMin(a:Int , b:Int) -> UInt8 {
        if a > b {
            return UInt8(b)
        }
        return UInt8(a)
    }
    func findMax(a:Int , b:Int) -> UInt8 {
        if a < b {
            return UInt8(b)
        }
        return UInt8(a)
    }
    func findMin(a:Double , b:Double) -> Double {
        if a > b {
            return Double(b)
        }
        return Double(a)
    }
    func findMax(a:Double , b:Double) -> Double {
        if a < b {
            return Double(b)
        }
        return Double(a)
    }
}
