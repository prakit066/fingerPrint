//
//  FPMainViewController.swift
//  print
//
//  Created by Prakit Kongkaew on 17/2/2561 BE.
//  Copyright © 2561 Conan. All rights reserved.
//

import Foundation
import UIKit
class FPMainViewController : FPBaseViewController {
 
    
   
    @IBOutlet weak var lView: UIView!
    
    @IBOutlet weak var rView: UIView!
    
    var vcLeft : UIViewController!
    var vcRight : UIViewController!
    override func loadView() {
        super.loadView()
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        vcLeft = imageEditView(nibName : "imageFingerView",bundle : nil,number : 0,delegate : self)
        //vcLeft.view.frame = lView.frame
        self.addChildViewController(vcLeft)
        lView.addSubview(vcLeft.view)
        
        vcRight = imageEditView(nibName : "imageFingerView",bundle : nil,number : 1 , delegate : self)
        //vcRight.view.frame = rView.frame
        self.addChildViewController(vcRight)
        rView.addSubview(vcRight.view)
        
    }
    func doneButton() {
        
    }
    
}
