//
//  Home.swift
//  print
//
//  Created by Prakit Kongkaew on 3/18/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

enum typeImageSelect : Int {
    
    case criminal = 0
    case exemple
    
//    func nameOfRow() -> String {
//        switch self {
//        case .criminal:
//            return "criminalFinger"
//        case .exemple:
//            return "exempleFinger"
//        default:
//            ""
//        }
//    }
    
}

class FPImportImageViewController: FPBaseViewController , UINavigationControllerDelegate, UIImagePickerControllerDelegate ,  UIPopoverPresentationControllerDelegate  {
    @IBOutlet weak var criminalImage: UIImageView!
    var type : typeImageSelect!
    @IBOutlet weak var exempleImage: UIImageView!
    @IBOutlet var criminalEdit: MJConfirmButton!
    @IBOutlet var exempleEdit: MJConfirmButton!
    
    let picker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.allowsEditing = false
        picker.delegate = self
        picker.sourceType = .photoLibrary
        criminalEdit.tag = 0
        exempleEdit.tag = 1
        criminalImage.transform = CGAffineTransform.identity.rotated(by: CGFloat(0))
       
        updateFont()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let image = FPAppData.criminalFingerImage {
            criminalImage.image = image
            
        }
        if let image = FPAppData.exempleFingerImage {
            exempleImage.image = image
        }
    }
    func updateFont() {
        criminalEdit.setTitle("แก้ไข", for: UIControlState())
        exempleEdit.setTitle("แก้ไข", for: UIControlState())
    }
    func importPicture() {
        
        present(picker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
        
        dismiss(animated: true, completion: nil)
        switch type! {
        case .criminal:
             criminalImage.image = image
             FPAppData.criminalFingerImage = image
        case .exemple:
             exempleImage.image = image
             FPAppData.exempleFingerImage = image
        
        }
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonSelect(_ sender: UIButton) {
       type =  typeImageSelect.init(rawValue: sender.tag)
        
        importPicture()
        
    }
    
    @IBAction func goToEdit(_ sender: UIButton) {
        var rgb :RGBA?
        switch sender.tag {
        case 0:
            rgb = FPAppData.criminalFingerRgb
            FPAppData.imageToSelect = typeImageSelect.init(rawValue: 0)
        case 1:
            rgb = FPAppData.exempleFingerRgb
            FPAppData.imageToSelect = typeImageSelect.init(rawValue: 1)
        default:
            rgb = nil
        }
        if let x = rgb {
            FPAppData.imageForEditRgb = rgb
        }else {
            return
        }
        
        let navigationController = self.navigationController
        if let viewController = navigationController?.storyboard?.instantiateViewController(withIdentifier: (AppStoryboardIdentifier.edit)) {
            navigationController?.show(viewController, sender: self)
        }

    }

    
}
extension FPImportImageViewController: MJPopoverViewControllerDelegate {
    
    func didDismissMJPopoverViewController(_ controller: MJPopoverViewController) {
        
    }
}



