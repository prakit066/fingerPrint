//
//  MJAppButton.swift
//  print
//
//  Created by Prakit Kongkaew on 3/27/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

import UIKit

class MJAppButton: UIButton {
    
    var titleFont: UIFont {
        return MJTextStyle.thai2.font(regular: isRegularHorizontalSizeClass)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareView()
    }
    
    class func button() -> Self {
        let button = self.init(type: .system)
        button.prepareView()
        return button
    }
    
    internal func prepareView() {
        
        titleLabel?.font = titleFont
        redrawBackgroundImage()
    }
    
    internal func redrawBackgroundImage() {
        
    }
    
    override var intrinsicContentSize : CGSize {
        var size = super.intrinsicContentSize
        size.height = ceil(titleFont.lineHeight) + 6
        size.width += size.height
        return size
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        titleLabel?.font = titleFont
        invalidateIntrinsicContentSize()
        redrawBackgroundImage()
    }
    
}

class MJConfirmButton: MJAppButton {
    
    override func prepareView() {
        super.prepareView()
        setTitleColor(MJColorStyle.gray, for: .disabled)
        setTitleColor(MJColorStyle.brown, for: UIControlState())
    }
    
    override func redrawBackgroundImage() {
        
        let contentHeight = CGFloat( 20.0)//intrinsicContentSize.height
        let inset = MJAppStyle.cornerRedius
        let size = CGSize(width: DGTCornerInsetResizableSize(inset).width, height: contentHeight)
        let image = UIImage(color: MJColorStyle.white, size: size, cornerInset: inset)
        let tintImage = image?.tinted(MJColorStyle.goldGradients)
        let resultImage = tintImage?.resizableImage(withCapInsets: UIEdgeInsetsMake(0, inset.topright, 0, inset.topleft))
        setBackgroundImage(resultImage, for: .normal)
        
        let disableImage = UIImage(color: MJColorStyle.gray, size: size, cornerInset: inset)
        let resultDisableImage = disableImage?.resizableImage(withCapInsets: UIEdgeInsetsMake(0, inset.topright, 0, inset.topleft))
        setBackgroundImage(resultDisableImage, for: .disabled)
    }
}

