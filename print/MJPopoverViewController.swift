//
//  MJPopoverViewController.swift
//  print
//
//  Created by Prakit Kongkaew on 3/26/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

import UIKit

@objc protocol MJPopoverViewControllerDelegate {
    
    @objc optional func didDismissMJPopoverViewController(_ controller: MJPopoverViewController)
}

class MJPopoverViewController: UIViewController {
    
    fileprivate(set) var contentViewController: UIViewController!
    
    let contentView: UIView = UIView()
    
    weak var delegate: MJPopoverViewControllerDelegate?
    var spaceWhenKeyboardShowing: CGFloat = 0
    
    required convenience init(contentViewController: UIViewController) {
        self.init()
        self.contentViewController = contentViewController
        modalPresentationStyle = .custom;
        self.transitioningDelegate = self
        contentView.frame = CGRect(origin: CGPoint.zero, size: contentViewController.preferredContentSize)
        contentView.addSubview(contentViewController.view)
        contentView.center = self.view.center
        addChildViewController(contentViewController)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentView.clipsToBounds = true
        view.addSubview(contentView)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(MJPopoverViewController.onKeyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(MJPopoverViewController.onKeyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.superview?.backgroundColor = UIColor.clear
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        delegate?.didDismissMJPopoverViewController?(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    deinit {
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        if let controller = contentViewController as? UINavigationController, let firstController = controller.topViewController {
            return firstController.preferredStatusBarStyle
        }
        return contentViewController.preferredStatusBarStyle
    }
    
    
    // MARK: -------------------------------------------------------
    // MARK: NSNotification For keyboard
    // MARK: -------------------------------------------------------
    
    internal func onKeyboardWillShow(_ notification: Notification) {
        moveKeyboard(true, forNotification: notification)
    }
    
    internal func onKeyboardWillHide(_ notification: Notification) {
        moveKeyboard(false, forNotification: notification)
    }
    
    internal func moveKeyboard(_ up: Bool, forNotification notification: Notification) {
        
        let userInfo = (notification as NSNotification).userInfo!
        
        // Get animation info from userInfo
        let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
        //        let animationCurve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as! UIViewAnimationCurve
        let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        if (contentView.frame.minY + contentView.bounds.height) > view.frame.height - keyboardEndFrame.height {
            UIView.animate(withDuration: animationDuration, delay: 0, options:.curveEaseIn, animations: {
                self.contentView.center.y = up ? self.view.center.y - self.spaceWhenKeyboardShowing : self.view.center.y
            }, completion: nil)
        }
    }
    
    // MARK: -------------------------------------------------------
    // MARK: InterfaceOrientations
    // MARK: -------------------------------------------------------
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
}

extension MJPopoverViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
}

extension MJPopoverViewController: UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)! as UIViewController
        
        let toController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)! as UIViewController
        
        let isPresent = toController.isBeingPresented
        let actionViewController = (isPresent ? toController : fromController) as! MJPopoverViewController
        let actionView = actionViewController.view
        let contentView = actionViewController.contentView
        
        let containerView = transitionContext.containerView
        actionView?.frame = containerView.frame
        actionView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.addSubview(actionView!)
        
        let dulation = self.transitionDuration(using: transitionContext)
        
        if isPresent {
            
            contentView.transform = CGAffineTransform.identity.scaledBy(x: 0.4, y: 0.4)
            UIView.animate(withDuration: dulation, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 8.0, options: [], animations: { () -> Void in
                
                actionView?.layer.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.595890411).cgColor
                contentView.transform = CGAffineTransform.identity
                
            }) { (success) -> Void in
                transitionContext.completeTransition(true)
            }
        } else {
            
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                contentView.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
            }, completion: { (success) -> Void in
                
                UIView.animate(withDuration: 0.4, animations: { () -> Void in
                    
                    actionView?.layer.backgroundColor = UIColor.clear.cgColor
                    contentView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
                    contentView.alpha = 0
                }, completion: { (success) -> Void in
                    transitionContext.completeTransition(true)
                })
            })
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
}

