//
//  FBMenuTableViewCell.swift
//  print
//
//  Created by Prakit Kongkaew on 3/25/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

import UIKit

class FPMenuTableViewCell : FPBaseTableViewCell{
    
    @IBOutlet var menuName: UILabel!
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        updateViewFont()
    }
    
    func updateViewFont(_ regular: Bool? = false) {
        
    }
    
    func updateElementOfCell(_ name : String) {
        menuName.text = name
    }
    

}
