//
//  FPBaseTableViewCell.swift
//  print
//
//  Created by Prakit Kongkaew on 3/25/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

import Foundation
import UIKit
class FPBaseTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.backgroundColor = UIColor.clear
        backgroundColor = UIColor.clear
       
    }
    
    @IBInspectable var showSeperateLine: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var gradientSeperateLine: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var seperateLineLeftInset: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var bgColor: UIColor? = UIColor.clear {
        didSet {
            setNeedsDisplay()
        }
    }
    
    func updateElementOfCell() {
        
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
    }
}
