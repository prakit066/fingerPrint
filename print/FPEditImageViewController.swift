//
//  FPEditImageViewController.swift
//  print
//
//  Created by Prakit Kongkaew on 3/27/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//


import UIKit
class FPEditImageViewController: FPBaseViewController{
    
    @IBOutlet var imageToEdit: UIImageView!
    var imageEditRgb : RGBA!
    @IBOutlet var auto: MJConfirmButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let imageEdit = FPAppData.imageForEditRgb {
            imageToEdit.image = imageEdit.toUIImage()
            imageEditRgb = imageEdit
        }
        updateFont()
    }
    func updateFont(){
        auto.setTitle("auto", for: UIControlState())
    }
    @IBAction func autoToGray(_ sender: UIButton) {
        imageEditRgb.toGrayScale()
        
        imageToEdit.image = imageEditRgb.toUIImage()
        switch FPAppData.imageToSelect! {
        case .criminal:
            FPAppData.criminalFingerImage = imageToEdit.image
        case .exemple :
            FPAppData.exempleFingerImage = imageToEdit.image
        }
    }
    
}
