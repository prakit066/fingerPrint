//
//  FPAppConstants.swift
//  print
//
//  Created by Prakit Kongkaew on 19/2/2561 BE.
//  Copyright © 2561 Conan. All rights reserved.
//


enum typeSourceImage : Int {
    
    case camera = 0
    case libery
    
    func getString() -> String {
        switch self {
        case .camera:
          return "กล้อง"
        case .libery:
         return "คลังรูปภาพ"
       
       }
    
    }
    static var getNumber : Int {
        get {
            return 2
        }
    }
}
