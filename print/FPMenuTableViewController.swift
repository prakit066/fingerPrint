//
//  FPMenuViewController.swift
//  print
//
//  Created by Prakit Kongkaew on 3/25/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//


import Foundation
import UIKit
import CoreGraphics

enum menuList : Int {
    
    case select = 0
    
    func nameOfRow() -> String {
        switch self {
        case .select : return "New Project"
        default:
            return ""
        }
        
    }
    func storyboardIdentifier() -> String {
        switch self {
        case .select :return AppStoryboardIdentifier.select
        default:
            return ""
        }
    }
    
    static var countOfItem: Int {
        return 1
    }
}
class FPMenuTableViewController: FPBaseTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelection = true
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.register(FPBaseTableViewCell.self, forCellReuseIdentifier: FPBaseTableViewCell.reuseIdentifier())
        tableView.canCancelContentTouches = false
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.countOfItem
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menu = menuList.init(rawValue: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: FPMenuTableViewCell.reuseIdentifier(), for: indexPath)  as! FPMenuTableViewCell
        cell.updateElementOfCell((menu?.nameOfRow())!)
        return  cell
    }
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let menu = menuList.init(rawValue: indexPath.row)
//        let navigationController = self.navigationController
//        if let viewController = navigationController?.storyboard?.instantiateViewController(withIdentifier: (menu?.storyboardIdentifier())!) {
//            navigationController?.show(viewController, sender: self)
//        }
//    }

    
}
