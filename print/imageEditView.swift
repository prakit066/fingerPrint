//
//  imageEditView.swift
//  print
//
//  Created by Prakit Kongkaew on 17/2/2561 BE.
//  Copyright © 2561 Conan. All rights reserved.
//

import UIKit

class imageEditView: FPBaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate ,UIActionSheetDelegate , UIToolbarDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var imageUse: UIImageView!
    var superView : FPMainViewController?? = nil
    @IBOutlet weak var newImage: UIButton!
    @IBOutlet weak var bg: UIImageView!
    var isShowAdjustView : Bool = false
    var isShowCropView : Bool = false
    var isShowScelling : Bool = false
    var space : CGFloat = 15
    var side : FPside?
    let imagePicker: UIImagePickerController! = UIImagePickerController()
    var imageRGB : RGBA? = nil
    var stackImage : [UIImage] = []
    var scelwidth : CGFloat = 500
    var sizeCropArea : rectunglePoint = rectunglePoint()
    var contrastValue :Double = 0.5
    var brightnessValue : Double = 0.5
    var panGesture : UIPanGestureRecognizer? = nil
    @IBOutlet weak var cropView: cropEditView!
   
    @IBOutlet weak var croparea: UIView!
    
    
    @IBOutlet weak var brightnessBar: UISlider!
    @IBOutlet weak var constrastBar: UISlider!
    @IBOutlet weak var adjustView: UIView!
    
    @IBOutlet weak var scallingButton: FPTabarButton!
    
    @IBOutlet weak var cropButton: FPTabarButton!
    
    @IBOutlet weak var adjustButton: FPTabarButton!
    
    
    
    var tabarAllButton : [FPTabarButton] = []
    init(nibName: String?, bundle: Bundle? ,number : Int ,delegate : FPMainViewController) {
        super.init(nibName: nibName, bundle: bundle)
        side = FPside(rawValue: number)!
        superView = delegate
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let frame = toolBar.frame
        adjustView.isHidden = !isShowAdjustView
        cropView.isHidden = !isShowCropView
        toolBar.frame = CGRect(x: frame.minX, y: frame.minY - space, width: frame.width, height: frame.height + space)
        setElement()
        toolBar.delegate = self
        tabarAllButton = [scallingButton,cropButton,adjustButton]
        // Do any additional setup after loading the view.
        addPanGesture(view: imageUse)
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 1.0
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setElement(){
        print(side)
        let inset = MJAppStyle.cornerRedius
        
        let image = UIImage(color: (side?.color())!, size: bg.frame.size, cornerInset: inset)
        let tintImage = image?.tinted((side?.colorGradients())!)
        let resultImage = tintImage?.resizableImage()
        bg.image = resultImage
    }
   
    func resetButton() {
        for  button  in tabarAllButton{
            button.setnormal()
        }
        
        if isShowAdjustView {
            isShowAdjustView = false
            stackImage.append(imageUse.image!)
            adjustView.isHidden = true
        }
        
        if isShowCropView {
            isShowCropView = false
            cropView.isHidden = true
        }
        isShowScelling = false
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {
        imagePicker.dismiss(animated: true, completion: {
            
        })
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: {
            // Anything you want to happen when the user selects cancel
        })
    }
    @IBAction func newImageAction(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: "เลือกภาพ", preferredStyle: .actionSheet)
        
        for index in 0..<typeSourceImage.getNumber {
            let k = typeSourceImage(rawValue: index)
            let defaultAction = UIAlertAction(title: k?.getString(), style: .default, handler: { (alert: UIAlertAction!) -> Void in
                self.getImagePicker(from: k!)
            })
            alertController.addAction(defaultAction)
        }
       
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        
        
        
        alertController.addAction(cancelAction)
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    func getImagePicker (from : typeSourceImage) {
        switch from {
        case .camera:
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            self.imagePicker.allowsEditing = true
        case .libery:
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            self.imagePicker.allowsEditing = true
           
        }
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageUse.contentMode = .scaleAspectFit
            let newWidth =  view.frame.size.width - 50
            scelwidth = newWidth
            imageUse.image = resizeImage(pickedImage, newWidth: newWidth)
            
            imageRGB = RGBA(image: imageUse.image!)
            stackImage.append(imageUse.image!)
        }
        
        imagePicker.dismiss(animated: true, completion: {
            self.newImage.isHidden = true
            
        })
    }
    
   
    @IBAction func contrastChange(_ sender: UISlider) {
        
        
            imageRGB = RGBA( image: stackImage[stackImage.endIndex-1])
            let c : Double  = ((Double(constrastBar.value) - 0.5) * 2.0) * 255.0
            let b : Double  = ((Double(brightnessBar.value) - 0.5) * 2.0) * 255.0
            imageRGB?.adjust(c: c , bri: b)
            var img = imageRGB?.toUIImage()
            imageUse.image = img
            imageRGB = nil
        
    }
    
    
    
    @IBAction func adjustButtom(_ sender: UIBarButtonItem) {
        
        if (stackImage.count > 0) &&  !isShowAdjustView {
            resetButton()
            isShowAdjustView = !isShowAdjustView
            
            
            if isShowAdjustView {
                
                adjustButton.setHighlight()
                adjustView.isHidden = false
            }
            
            
        }else if isShowAdjustView {
            
        }
      
    }
    func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    
    @IBAction func cropButtom(_ sender: UIBarButtonItem) {
        if (stackImage.count == 0){
            return
        }
        if isShowCropView {
            print(sizeCropArea.size,1)
            print(imageUse.frame,2)
            let croppedCGImage = imageUse.image?.cgImage?.cropping(to: cropArea1)
            let croppedImage = UIImage(cgImage: croppedCGImage!)
            imageUse.image = croppedImage
            resetButton()
            cropView.isHidden = true
            panGesture?.isEnabled = false
            stackImage.append(imageUse.image!)
            
        }
        else {
            resetButton()
            isShowCropView = true
            cropView.isHidden = false
            cropButton.setHighlight()
            view.bringSubview(toFront: cropView)
            let factor = imageUse.image!.size.height/view.frame.height
            let frame = imageUse.imageFrame()
            let frameScale = scrollView.frame
            let frame2 = imageUse.frame
            let frame1 = CGRect(x: frame.minX +  frame2.minX, y: frame.minY +  frame2.minY, width: frame.width, height: frame.height)
            
            print("okck",frame ,frame2,frame1,factor)
            sizeCropArea.setSize(size: frame1)
            croparea.frame = frame1
            panGesture?.isEnabled = true
            
        }
        
        
    }
   
    
    func addPanGesture(view: UIView) {

        panGesture = UIPanGestureRecognizer(target: self, action: #selector(imageEditView.handlePan(sender:)))

        view.addGestureRecognizer(panGesture!)
    }
    
    // Refactor
    func handlePan(sender: UIPanGestureRecognizer) {
         if !isShowCropView {
            return
        }
        
        let view = sender.view
        var point1 =  sender.location(in: view)
        point1.x = point1.x + (view?.frame.minX)!
        point1.y = point1.y + (view?.frame.minY)!
        switch sender.state {
        case .began :
            sizeCropArea.setPoint(point: point1)
            
        case .changed:
            changeArea(view: view!, sender: sender)
        case .ended :
            let frame = croparea.frame
            sizeCropArea.setSize(size: frame)
        default:
            break
        }
    }
    
    func changeArea(view: UIView, sender: UIPanGestureRecognizer) {
        let trans = sender.translation(in: view)
        
        print(sizeCropArea.direc)
        if sizeCropArea.direc == nil {
            return
        }
        switch sizeCropArea.direc! {
        case .N:
            changeArea2(x: 0.0, y: trans.y, w: 0, h: trans.y)
        case .NE:
            changeArea2(x: 0, y: trans.y, w: -trans.x, h: trans.y)
        case .E :
            changeArea2(x: 0, y: 0, w: -trans.x, h: 0)
        case .SE:
            changeArea2(x: 0, y: 0, w: -trans.x, h: -trans.y)
        case .S :
            changeArea2(x: 0, y: 0, w: 0, h: -trans.y)
        case .SW:
            changeArea2(x: trans.x, y: 0, w: trans.x, h: -trans.y)
        case .W:
            changeArea2(x: trans.x, y: 0, w: trans.x, h: 0)
        case .NW:
            changeArea2(x: trans.x, y: trans.y, w: trans.x, h: trans.y)
        case .CC:
            changeArea2(x: trans.x, y: trans.y, w: 0, h: 0)
        }
    }
    
    func changeArea2(x : CGFloat , y : CGFloat ,w : CGFloat , h:CGFloat) {
        let frame = sizeCropArea.size
        croparea.frame = CGRect(x: frame.minX + x, y: frame.minY + y, width: frame.size.width - w, height: frame.size.height - h)
    }
    
    var cropArea1:CGRect{
        get{
            let factor = imageUse.image!.size.width/view.frame.width
            let imageFrame = imageUse.imageFrame()
            let frame = imageUse.frame
            let multiH = frame.height / imageFrame.height
            let multiW = frame.width / imageFrame.width
            let x = (sizeCropArea.size.origin.x - imageFrame.minX - frame.minX)
            let y = (sizeCropArea.size.origin.y - imageFrame.minY - frame.minY)
            let width = sizeCropArea.size.width
            let height = sizeCropArea.size.height
            print(x,y,width,height)
            print(imageUse.frame,imageFrame,sizeCropArea.size)
            return CGRect(x: x, y: y, width: width, height: height)
        }
    }
    @IBAction func scellingButtonAction(_ sender: Any) {
        if isShowScelling {
            resetButton()
            
            scrollView.minimumZoomScale = 1.0
            scrollView.maximumZoomScale = 1.0
            scrollView.isScrollEnabled = false
            
        }else {
            resetButton()
            isShowScelling = true
            scallingButton.setHighlight()
            scrollView.minimumZoomScale = 1.0
            scrollView.maximumZoomScale = 5.0
            scrollView.isScrollEnabled = true
        }
    
    }
    @IBAction func scelling(_ sender: UIPinchGestureRecognizer) {
       
        
        if (stackImage.count == 0){
            return
        }
        if !isShowScelling {
            return
        }
        print(sender.scale)
        if sender.scale < 1 {
            if scelwidth > 150 {
                scelwidth = scelwidth * sender.scale
                imageUse.image = resizeImage(stackImage[stackImage.count - 1], newWidth: scelwidth)
            }
        }else {
            
            if scelwidth < 1000 {
                scelwidth = scelwidth * sender.scale
                imageUse.image = resizeImage(stackImage[stackImage.count - 1], newWidth: scelwidth)
            }
        }
        
    }
    
    //MARK:- EXTENSION FOR UIIMAGE
   
}
extension UIImage {
    var uncompressedPNGData: Data      { return UIImagePNGRepresentation(self)!        }
    var highestQualityJPEGNSData: Data { return UIImageJPEGRepresentation(self, 1.0)!  }
    var highQualityJPEGNSData: Data    { return UIImageJPEGRepresentation(self, 0.75)! }
    var mediumQualityJPEGNSData: Data  { return UIImageJPEGRepresentation(self, 0.5)!  }
    var lowQualityJPEGNSData: Data     { return UIImageJPEGRepresentation(self, 0.25)! }
    var lowestQualityJPEGNSData:Data   { return UIImageJPEGRepresentation(self, 0.0)!  }
}
extension UIImageView{
    func imageFrame()->CGRect{
        let imageViewSize = self.frame.size
        guard let imageSize = self.image?.size else{return CGRect.zero}
        let imageRatio = imageSize.width / imageSize.height
        let imageViewRatio = imageViewSize.width / imageViewSize.height
        
        if imageRatio < imageViewRatio {
            let scaleFactor = imageViewSize.height / imageSize.height
            let width = imageSize.width * scaleFactor
            let topLeftX = (imageViewSize.width - width) * 0.5
            return CGRect(x: topLeftX, y: 0, width: width, height: imageViewSize.height)
        }else{
            let scalFactor = imageViewSize.width / imageSize.width
            let height = imageSize.height * scalFactor
            let topLeftY = (imageViewSize.height - height) * 0.5
            return CGRect(x: 0, y: topLeftY, width: imageViewSize.width, height: height)
        }
    }
}

class CropAreaView: UIView {
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return false
    }
    
}
extension imageEditView: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageUse
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        //updateConstraintsForSize(view.bounds.size)
    }
    
}
