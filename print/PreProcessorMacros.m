//
//  PreProcessorMacros.m
//  print
//
//  Created by Prakit Kongkaew on 3/27/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

#import "PreProcessorMacros.h"

#ifdef DEBUG
BOOL const DEBUG_BUILD = YES;
#else
BOOL const DEBUG_BUILD = NO;
#endif
