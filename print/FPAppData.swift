//
//  FPAppData.swift
//  print
//
//  Created by Prakit Kongkaew on 3/25/2560 BE.
//  Copyright © 2560 Conan. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics
class FPAppData {
    
    fileprivate var _criminalFingerImage : UIImage!
    fileprivate var _criminalFingerRgb : RGBA!
    fileprivate var _exempleFingerImage : UIImage!
    fileprivate var _exempleFingerRgb : RGBA!
    fileprivate var _imageForEditRgb : RGBA!
    fileprivate var _imageToSelect : typeImageSelect?
    class var shared: FPAppData {
        
        struct Static {
            static let instance: FPAppData = FPAppData()
        }
        return Static.instance
    }
    init() {
        
    }
    class var criminalFingerImage : UIImage! {
        set {
            shared._criminalFingerImage = newValue
            shared._criminalFingerRgb = RGBA.init(image: newValue)
        }
        get{
            return shared._criminalFingerImage
        }
    }
    class var exempleFingerImage : UIImage! {
        set {
            
            shared._exempleFingerImage = newValue
            shared._exempleFingerRgb = RGBA.init(image: newValue)
        }
        get{
            return shared._exempleFingerImage
        }
    }
    class var criminalFingerRgb : RGBA! {
        set {
            shared._criminalFingerRgb = newValue
            shared._criminalFingerImage = newValue.toUIImage()
        }
        get {
            return shared._criminalFingerRgb
        }
    }
    class var exempleFingerRgb : RGBA! {
        set {
            shared._exempleFingerRgb = newValue
            shared._exempleFingerImage = newValue.toUIImage()
        }
        get {
            return shared._exempleFingerRgb
        }
    }
    class var imageForEditRgb : RGBA! {
        set{
            shared._imageForEditRgb = newValue
        }get{
            return shared._imageForEditRgb
        }
    }
    class var imageToSelect : typeImageSelect?{
        set {
            shared._imageToSelect = newValue
        }
        get{
          return shared._imageToSelect
        }
    }
}
